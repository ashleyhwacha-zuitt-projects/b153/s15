// //favorite food
// let favFood = "pizza";
// //sum of two numbers
// let addNum = 150 + 9 ;
// // product 
// let multiNum = 100 * 90 ;
// // bolean active
// let isActive = true;
// // Favorite  restaurants
// let favRestaurants = ['burger king', 'nandos', 'chic fillA', 'rOCCOS', 'sTAR BUCKS'];

// let bestArtist = {
// 	firstName: "Bob",
// 	lastName: "Dylan",
// 	stageName: "Bob Dylan",
// 	birthday: "09/03/72",
// 	age: 64,
// 	bestAlbum: "One River",
// 	bestTVShow: "Main man",
// 	bestSong: "one day",
// 	isActive: false
// };
// console.log(favFood, addNum, multiNum, isActive, favRestaurants, bestArtist);
			
// 			function divideNum(num1, num2){
// 				let divided = num1/num2;
// 				console.log(divided);

// 			    return divided;
// 			};

// 			let quo = divideNum(6, 3);

// console.log("The result of the division is : "+ quo);


// console.log( true != "true");
// //with the type conversion: true was converted to 1
// // "true was converted into a number but the results is NaN"

// console.log("0" != false);
// //type conversion: "0" converted to 0
// //false converted to 0
// //0 is equal to 0 - is NOT equal

// console.log("5" !== 5);
// //true operands are inequal because they have different types

// console.log(5 !== 5);
// //false - operands are equal, they have the same value and they have the same type

// console.log("true" !== true);
// //true - operands are unequal because they have diff types

// //Relational Comparison operators
//   // A comparison operator which will check the rlationship between the operands

//   let x = 500;
//   let y = 700;
//   let w = 8000;
//   let numString3 = "5500";

//   //Greater than (>)

//   console.log( x > y);
//   console.log( w > y);

//   // less than (<)
//   console.log(w < y);
//   console.log(y < y);
//   console.log(x < 1000);
//   console.log(numString3 < 1000); //false - forced coercion
//   to //change the string to number

//   console.log(numString3 < "Jose"); //true - "5500" < "Jose" -that is erratic

//   // Graeter than or Equal to (>=)
//   console.log(w >= 10000); // false
//   console.log(y >= x);

//   // less than or equal to (<=)

//   console.log(x <=y); //true
//   console.log(w <= x); //false

//   // logical Operators - AND OR
//      // AND operator (&&)
//      //It means that both operands on the left and right or all operands added must be true or results to true.

//      /*
//         T && T = TRUE
//         F && T = FALSE
//         T && F = FALSE
//         F && F = FALSE
//      */

//      let isAdmin = false;
//      let isRegistered = true;
//      let isLegalAge = true;

//      // to get authorisation you should be an admin and registered
//      let authorization1 = isAdmin && isRegistered;

//      console.log(authorization1);

//      // to get authorization you be at legal age and registered

//      let authorization2 = isLegalAge && isRegistered;
//      console.log(authorization2);

//      // to get authorization 
//      let authorization3;

//      let requiredLevel = 95;
//      let requiredAge = 18;

//      let authorization4 = isRegistered && requiredLevel === 25;

//      console.log(authorization4);

//      let authorization5 = isRegistered && isLegalAge && requiredLevel === 95;

//      console.log(authorization5);

//      let userName = "gamer2001";
//      let userName = "shadow1991";
//      let userAge1 = 15;
//      let userAge2 = 30;

//      let registration1 = userName1 > 8 && userAge1 >= requiredAge;

//      //lenght is a property of string which determines the number of character in the string
//      console.log(registration1);

//      let registration2 = userName2.length > 8 && userAge2 >= requiredAge;

//      console.log(registration2);

//      //OR Operator (// Double Pipe)
//         //or operator returns true if at least one of the operands are true

//         /*
//              T// T = true
//              F // T = true
//              T // F = TRUE

//         */

//      let userLevel1 = 100;
//      let userLevel2 = 65;

//      let guildRequirements1 = isRegistered && userLevel1 >= requiredLevel && userAge1 >= requiredAge;
//       console.log(guildRequirements); //flase


//           let guildRequirements2 = isRegistered || userLevel1 >= requiredLevel || userAge1 >= requiredAge;
//       console.log(guildRequirements); //true

//       let guildRequirements3 = userLevel1 >= requiredLevel || userAge1 >= requiredAge;
//        console.log(guildRequirements3);

//        let guildAdmin = isAdmin || userLevel2 >= requiredLevel;

//        console.log(guildAdmin);

//        // Not Operator
//          //turns boolean into opposite

//          /*

//           T =F
//           F = T
//          */

//          let guildAdmin2 = !isAdmin || userLevel1 >= requiredLevel;

//           console.log(guildAdmin2); //true

//           console.log(!isAdmin);
//           console.log(!isRegistered);


//           // if-else 
//             // if statement will run a code block if the condition specified is true or the results into true

//             let userName3 = "crusader_1993";
//             let userLevel13 = 25;
//             let userAge3 = 20;

//             // if(true){
//             // 	alert("we just run the if condition");
//             // }
//             if (userName3.length > 10){
//             	console.log("welcome to Game online")
//             }

//             if(userName3.length > 10 && isRegistered && isAdmin){
//             	console.log("Thank you for joining the Admin");
//             }else{
//             	console.log("you too strong to be noob. :(");
//             };

//             // else if executes a statement if the previous condition is false

//             if(userName3.length >= 10 && userLevel3 <= 25 && userAge3 >= requiredAge){
            	
// 	          console.log("Thank you for joinign the moobies guild");


//             }else if(userLevel3 > 25){
//             	console.log("you too strong to be a noob");
//             }else if(userAge3 < requiredAge){
//             	console.log("Youre too young to join th guild")
//             }

//             //else statement will be run if the condition given is false or results to false




//             // if else in function

//             // typeof keyword returns a string which tells the type of dat that follows it
//             function addNum(num1, num2){
//             	if (typeof num1 === "number" && typeof num2 ==="number"){
//             		console.log('Run if both arguments passed are numbers')
//             	}else{
//             		console.log("one or both of the arguments are not numbers")
//             	}
//             }

//             addNum(5, 2);

            function login(username, password){

        if (typeof username === "string" && typeof password === "string" && password.length >= 8 ){
            		console.log("both Arguments are strings and password is correct number of characters")
                    console.log("password length is correct size")
            		/*nested if-else
            		  will run if the parent if statement is able to agree to accomplish its condition
            		*/
            		
            	}
            	else{
            		alert("credentials too short")
            	}
            }

           // login("ashley", "ashleypassword");


            //mini activity

            function dayShirtColor(userInput){
              if(userInput == "Monday"){
              	shirtColor = "Black";
              }else if(userInput == "Tuesday"){
                  shirtColor = "Green";
              }else if(userInput == "Wednesday"){
                  shirtColor = "Yellow";
              }else if(userInput == "Thursday"){
                  shirtColor = "Red";
              }else if(userInput == "Friday"){
                  shirtColor = "Violet";
              }else if(userInput == "Saturday"){
                  shirtColor = "Blue";
              }else if(userInput == "Sunday"){
                  shirtColor = "White";
              }else{ 
               
               return console.log("Invalid Input. Enter a valid day of the week");
              }

              console.log("Today is "+ userInput +", wear "+shirtColor);
            }

            dayShirtColor("tuesday");

            /* swithch statements

              -is an alternative to an if , else if, else tree.Where the data being evaluated or checked is of an

              snytax 
               swith(expression/condition){
	      case value:
	           statement;
	           break;
	      case value:
	            statement;
	            break;


               }
              */


              let hero = "Hercules";
               switch(hero){
               	   case "Jose Rizal":
               	       console.log("National Hero of the Philiipines");
               	       break;
               	   case "George Washington":
               	   console.log("Hero of the American REvo");
               	        break;

               	    default :
               	        console.log("invalid input");
               	        break;
               };


               let role = "Admin";

               function roleChecker(role){
               	switch(role){
               		case "Admin":
               		     console.log("Welcome Admin, to the Dashboard");
               		     break;
                     case "User" :
                           console.log("You are not authorised to view this page");
                      case "Guest" : 
                           console.log("go to registration page to register");
                           break;
                      default:
                           console.log("invalid Role");
               	        break;

               	}
               }

               roleChecker("User");

               /*
               Ternary operator
                shorthad for if else

                syntax :

                   condition? if-statement : else-statement
               */

               let price = 5000;

               // if (price > 1000){
               // 	console.log("Price is over 1000");
               // }else{
               // 	console.log("Price is less than 1000");
               // }


               price > 1000 ? console.log("Price is over 1000") : console.log("Price is less than 1000");

               let hero2 = "Goku";

               hero2 === "Vegeta" ? console.log("You are the privce of all seiyans") : console.log("youre not the prince");

               let villain = "Harvey Dert";

               villain === "Harvey Debt" ? console.log("yu are");

               /*ternary meant to have if else in one statement*/

               villain === "Two Face"
               ? console.log("You lived long enough to be a villain") : console.log("Not quite vilain yet");

               let robin1 = "Dick Grayson";
               let currentRobin = "Tin Drake";

               /*TERNARY not meant for complex if else trees, its adv over regular if else is not that it is only short , but also operation implicity returns  without return keyword*/

               let isFirstRobin = currentRobin === robin1 ? true : false;
               console.log(isFirstRobin);

               //Else if with ternary operator

               let a = 7;
               a === 5 ? console.log("A")
                       :(a === 10 ? console.log("A is 10"): console.log("A is not 5 or 10"));